package br.com.steamxqdl.domain;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "generos")
public class Genero {
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
 
    @NotBlank
    @Size(min = 2, max = 60)
    @Column(nullable = false, length = 60)
    private String nome;
 
    @NotBlank
    @Column(nullable = false)
    private String descricao;
 
    @OneToMany(mappedBy = "genero", cascade = CascadeType.ALL)
	private List<Jogo> jogos;
 
    public long getId() {
        return id;
    }
 
    public void setId(long id) {
        this.id = id;
    }
 
    public String getNome() {
        return nome;
    }
 
    public void setNome(String nome) {
        this.nome = nome;
    }
 
    public String getDescricao() {
        return descricao;
    }
 
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
 
    public List<Jogo> getJogos() {
        return jogos;
    }
 
    public void setJogos(List<Jogo> jogos) {
        this.jogos = jogos;
    }

}
