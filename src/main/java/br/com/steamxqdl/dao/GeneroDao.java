package br.com.steamxqdl.dao;

import java.util.List;

import br.com.steamxqdl.domain.Genero;

public interface GeneroDao {

	void salvar(Genero genero);

	List<Genero> recuperar();

	Genero recuperarPorID(long id);

	void atualizar(Genero genero);

	void excluir(long id);

}