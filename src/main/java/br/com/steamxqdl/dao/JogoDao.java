package br.com.steamxqdl.dao;

import java.util.List;

import br.com.steamxqdl.domain.Jogo;

public interface JogoDao {

	void salvar(Jogo jogo);

	List<Jogo> recuperarPorGenero(long generoId);

	Jogo recuperarPorGeneroIdEJogoId(long generoId, long jogoId);

	void atualizar(Jogo jogo);

	void excluir(long jogoId);

}