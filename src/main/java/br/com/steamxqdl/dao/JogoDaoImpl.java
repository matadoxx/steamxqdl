package br.com.steamxqdl.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.steamxqdl.domain.Jogo;

@Repository
public class JogoDaoImpl implements JogoDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public void salvar(Jogo jogo) {
		em.persist(jogo);
	}

	@Override
	public List<Jogo> recuperarPorGenero(long generoId) {
		return em.createQuery("select j from Jogo j where j.genero.id = :generoId", Jogo.class)
				.setParameter("generoId", generoId).getResultList();
	}

	@Override
	public Jogo recuperarPorGeneroIdEJogoId(long generoId, long jogoId) {
		return em
				.createQuery("select j from Jogo j where j.genero.id = :generoId and j.id = :jogoId", Jogo.class)
				.setParameter("generoId", generoId).setParameter("jogoId", jogoId).getSingleResult();
	}

	@Override
	public void atualizar(Jogo jogo) {
		em.merge(jogo);
	}

	@Override
	public void excluir(long jogoId) {
		em.remove(em.getReference(Jogo.class, jogoId));
	}

}