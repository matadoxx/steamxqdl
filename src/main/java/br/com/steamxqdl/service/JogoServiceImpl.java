package br.com.steamxqdl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.steamxqdl.dao.JogoDao;
import br.com.steamxqdl.domain.Jogo;

@Service
@Transactional
public class JogoServiceImpl implements JogoService {

	@Autowired
	private JogoDao jogoDao;

	@Autowired
	private GeneroService generoService;

	@Override
	public void salvar(Jogo jogo, long generoId) {
		jogo.setGenero(generoService.recuperarPorId(generoId));
		jogoDao.salvar(jogo);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Jogo> recuperarPorGenero(long generoId) {
		return jogoDao.recuperarPorGenero(generoId);
	}

	@Override
	@Transactional(readOnly = true)
	public Jogo recuperarPorGeneroIdEJogoId(long generoId, long jogoId) {
		return jogoDao.recuperarPorGeneroIdEJogoId(generoId, jogoId);
	}

	@Override
	public void atualizar(Jogo jogo, long generoId) {
		jogo.setGenero(generoService.recuperarPorId(generoId));
		jogoDao.atualizar(jogo);
	}

	@Override
	public void excluir(long generoId, long jogoId) {
		jogoDao.excluir(recuperarPorGeneroIdEJogoId(generoId, jogoId).getId());
	}

}