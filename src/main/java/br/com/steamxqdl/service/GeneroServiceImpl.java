package br.com.steamxqdl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.steamxqdl.dao.GeneroDao;
import br.com.steamxqdl.domain.Genero;

@Service
@Transactional
public class GeneroServiceImpl implements GeneroService {

	@Autowired
	private GeneroDao generoDao;

	@Override
	public void salvar(Genero genero) {
		generoDao.salvar(genero);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Genero> recuperar() {
		return generoDao.recuperar();
	}

	@Override
	@Transactional(readOnly = true)
	public Genero recuperarPorId(long id) {
		return generoDao.recuperarPorID(id);
	}

	@Override
	public void atualizar(Genero genero) {
		generoDao.atualizar(genero);
	}

	@Override
	public void excluir(long id) {
		generoDao.excluir(id);
	}

}