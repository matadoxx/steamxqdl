package br.com.steamxqdl.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class ConfiguracaoSecurity extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
			.antMatchers("/webjars/**", "/css/**").permitAll() 
			.antMatchers("/", "/home").permitAll()
            .antMatchers("/jogos/*", "/jogos/**", "/jogos/***").hasAnyRole("ADMIN")
            .antMatchers("/generos/*", "generos/**", "generos/***").hasAnyRole("ADMIN")
            .antMatchers("/jogos").hasAnyRole("ADMIN")
            .antMatchers("/generos").hasAnyRole("ADMIN")
            .anyRequest()
            .authenticated()
        	.and()
        .formLogin()
        	.loginPage("/login")
        	.permitAll()
			.and()
		.logout()
			.permitAll()
			.and()
		.exceptionHandling()
			.accessDeniedPage("/erro")
			.and()
		.rememberMe();
	  }
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {
		
		PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		
		builder.inMemoryAuthentication().withUser("admin").password(encoder.encode("admin")).roles("ADMIN")
		.and()
		.withUser("user").password(encoder.encode("user")).roles("USER");
	}
	
}
