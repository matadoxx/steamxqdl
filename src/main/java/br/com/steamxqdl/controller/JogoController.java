package br.com.steamxqdl.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.steamxqdl.domain.Jogo;
import br.com.steamxqdl.service.JogoService;

@Controller
@RequestMapping("generos/{generoId}/jogos")
public class JogoController {

	@Autowired
	private JogoService jogoService;

	@GetMapping("/listar")
	public ModelAndView listar(@PathVariable("generoId") long generoId, ModelMap model) {
		model.addAttribute("jogos", jogoService.recuperarPorGenero(generoId));
		model.addAttribute("generoId", generoId);
		return new ModelAndView("/jogo/list", model);
	}

	@GetMapping("/cadastro")
	public String preSalvar(@ModelAttribute("jogo") Jogo jogo, @PathVariable("generoId") long generoId) {
		return "/jogo/add";
	}

	@PostMapping("/salvar")
	public String salvar(@PathVariable("generoId") long generoId, @Valid @ModelAttribute("jogo") Jogo jogo,
			BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/jogo/add";
		}

		jogoService.salvar(jogo, generoId);
		attr.addFlashAttribute("mensagem", "Jogo salvo com sucesso.");
		return "redirect:/generos/" + generoId + "/jogos/listar";
	}

	@GetMapping("/{jogoId}/atualizar")
	public ModelAndView preAtualizar(@PathVariable("generoId") long generoId,
			@PathVariable("jogoId") long jogoId, ModelMap model) {
		Jogo jogo = jogoService.recuperarPorGeneroIdEJogoId(generoId, jogoId);
		model.addAttribute("jogo", jogo);
		model.addAttribute("generoId", generoId);
		return new ModelAndView("/jogo/add", model);
	}

	@PutMapping("/salvar")
	public String atualizar(@PathVariable("generoId") long generoId, @Valid @ModelAttribute("jogo") Jogo jogo,
			BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/jogo/add";
		}

		jogoService.atualizar(jogo, generoId);
		attr.addFlashAttribute("mensagem", "Jogo atualizado com sucesso.");
		return "redirect:/generos/" + generoId + "/jogos/listar";
	}

	@GetMapping("/{jogoId}/remover")
	public String remover(@PathVariable("generoId") long generoId, @PathVariable("jogoId") long jogoId,
			RedirectAttributes attr) {
		jogoService.excluir(generoId, jogoId);
		attr.addFlashAttribute("mensagem", "Jogo excluído com sucesso.");
		return "redirect:/generos/" + generoId + "/jogos/listar";
	}

}